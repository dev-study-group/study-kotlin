import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Chapter4LambdaJava {

    public static void postponeCompution(int delay, Runnable runnable) {

    }

    public static void main(String[] args) {
        List<List<String>> list = Arrays.asList(Arrays.asList("a", "b", "c"), Arrays.asList("d", "e", "f"), Arrays.asList("g", "h", "i"));

        List<String> collect = list.stream().flatMap(List::stream).collect(Collectors.toList());


    }
}
