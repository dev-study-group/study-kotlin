package com.kakao.studykotlin

import Chapter4LambdaJava
import org.junit.Test

class Chapter4Lambda {

    private val people: List<Person>
    private val errors: List<String>
    private fun salute() = println("Salute")

    private val getAge = { p: Person -> p.age }

    init {
        people = listOf(Person("name-1", 1), Person("name-2", 2), Person("name-3", 3), Person("name-x", 3))
        errors = listOf("403 Forbidden", "404 Not Found")
    }

    @Test
    fun testLambda() {
        // public inline fun <T, R : Comparable<R>> Iterable<T>.maxBy(selector: (T) -> R): T?
        people.maxBy { p: Person -> p.age }    //
        people.maxBy { it.age }     //
        people.maxBy(Person::age)   // 맴버 잠초
        people.maxBy(getAge)        // 람다 변수


        val nextAction = ::sendEmail // 위임 함수에 대한 참조를 저장

        val createPerson = createPerson("name-4", 4)

        nextAction(createPerson, "text message")


    }

    @Test
    fun testLambdaParams() {
        people.joinToString(separator = " ") { it.name }
        people.joinToString(separator = " ", transform = { it.name })

    }

    @Test
    fun testUseFunctionParameterInLambda() {
        printMessagesWithPrefix(errors, "Error")
    }

    @Test
    fun testTopLevelFunctionReference() {
        kotlin.run(::salute) // 최상위 함수 참조 ( Person::age -> member reference )
    }

    @Test
    fun testCollectionFunctionalApi() {
//        public inline fun <T, R> Iterable<T>.map(transform: (T) -> R): List<R>
        val list = listOf(1, 2, 3, 4, 5)
        println(list.map { it * it })

        println(list.filter { it > 2 }.map { it * 2 })
        println(list.all(overTwo)) // println(!list.any(overTwo))
        println(list.any(overTwo)) // println(!list.all(overTwo))

        println(people.groupBy { it.age })

        val strings = listOf("abc", "def")
        println(strings.flatMap { it.toList() })

        val even: IntArray = intArrayOf(2, 4, 6)
        val odd: IntArray = intArrayOf(1, 3, 5)
        val int2DArray: Array<IntArray> = arrayOf(even, odd)
        println(int2DArray.flatMap { it.toList() })


        val nameList = people.asSequence()
                .map(Person::name)
                .filter { it.startsWith("n") }
                .toList()
        println(nameList)

        val nameList2 = people
                .map(Person::name)
                .filter { it.startsWith("n") }
        println(nameList2)


        Chapter4LambdaJava.postponeCompution(1000) { println(42) }

    }

    @Test
    fun testApply() {
//        public inline fun <T> T.apply(block: T.() -> Unit): T {
//        apply()는 함수를 호출하는 객체를 이어지는 블록의 리시버 로 전달하고, 객체 자체를 반환

        val book = Book()
        book.name = "book name"
        book.author = "book author"


        val book2: Book = Book().apply {
            name = "book2 name"
            author = "boo2 author"
        }
        println("book 1 = $book")
        println("book 2 = $book2")
    }

    @Test
    fun testWith() {
//        public inline fun <T, R> with(receiver: T, block: T.() -> R): R
//        with() 함수는 인자로 받는 객체를 이어지는 블록의 리시버로 전달하며, 블록의 결과값을 반환합니다.


        val book: Book = with(Book()) {
            name = "book name"
            author = "book author"
            this
        }
        println("book = $book")
    }

    private val overTwo = { i: Int -> i > 2 }

    private fun printMessagesWithPrefix(message: Collection<String>, prefix: String) {
        message.forEach {
            println("$prefix $it") // prefix 사용
        }
    }

    private fun sendEmail(person: Person, message: String) {
        println("send email to $person, messages is $message")
    }

    private val createPerson = ::Person // 생성자 참조


    data class Person(val name: String, val age: Int)
    class Book {
        lateinit var name: String
        lateinit var author: String
    }
}