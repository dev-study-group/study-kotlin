package com.kakao.studykotlin

import org.junit.Test
import java.time.LocalDate

class Chapter7Operator {
    private val p1 = Point(1, 2)
    private val p2 = Point(3, 4)
    private var p3 = Point(3, 4)

    @Test
    fun ex7_1_1() {
        assert(p1 + p2 == p1.plus(p2))
    }

    @Test
    fun ex7_1_2() {
        assert(p1 - p2 == p1.minus(p2))
    }

    @Test
    fun ex7_1_3() {
//        p1 += p2
//        p3 += p2
    }

    @Test
    fun ex7_1_6() {
        print(-p3)
        print(--p3)
    }

    @Test
    fun indexOperator() {
        val p1 = Point(10, 20)
        println(p1[1])

        val p2 = MutablePoint(10, 20)
        p2[1] = 42
        println(p2)
    }

    @Test
    fun inOperator() {
        val rect = Rectangle(Point(10, 20), Point(50, 50))
        println(Point(20, 30) in rect)
        println(Point(5, 5) in rect)
    }

    @Test
    fun iteratorOperator() {
        val newYear = LocalDate.ofYearDay(2017, 1)
        val daysOff = newYear.minusDays(1)..newYear
        for (dayOff in daysOff) { println(dayOff) }
    }

    @Test
    fun destructingDeclaration() {
        var (name, extension) = splitFilename("yuki.b")
        println("name is $name, extension is $extension")

        val map = mapOf("oracle" to "java", "jetbrain" to "kotlin")
        println(printEntries(map))
    }

    @Test
    fun lazyInit() {
        val p = Crew("Alice")
        p.emails
        p.emails
        p.lazyEmails
    }
}

/** 7.1.1 관례를 따르는 멤버 함수 */
data class Point(val x: Int, val y: Int) {
    operator fun plus(other: Point): Point {
        // operator 키워드를 사용하여 관례를 따르는 함수임을 명시한다.
        // 잘못 관례 메소드 명을 사용한 경우 : operator modifier is required ... 오류
        return Point(x + other.x, y + other.y)
    }

    override fun equals(obj: Any?): Boolean { /** 7.2.1 비교연산자 관례, equals*/
        if (obj === this) return true
        if (obj !is Point) return false
        return obj.x == x && obj.y == y
    }
}

/** 7.1.2 관례를 따르는 함수를 확장함수로 선언
 * 관례를 따르는 함수를 구현할때는 확장함수로 구현하는것이 일반적 */
operator fun Point.minus(other: Point): Point {
    return Point(x - other.x, y - other.y)
}

/** 7.1.3 피연산자의 타입이 다른 연산자 */
operator fun Point.times(scale: Double): Point {
    return Point((x * scale).toInt(), (y * scale).toInt())
}

/** 7.1.4 반환 타입이 다른 연산자 */
operator fun Char.times(count: Int): String {
    return toString().repeat(count)
}

/** 7.1.6 단항 연산자*/
operator fun Point.unaryMinus(): Point {
    return Point(-x, -y)
}
operator fun Point.dec(): Point {
    return Point(-(-x), -(-y))
}


/** 7.2.2 comparable*/

class Person(val firstName: String, val lastName: String) : Comparable<Person> {
    override fun compareTo(other: Person): Int {
        // 코틀린 표준 라이브러리 함수.
        // 필드를 직접 비교하는것이 속도에는 더 좋다.
        return compareValuesBy(this, other, Person::lastName, Person::firstName)
    }
}



/** 7.3.1 get, set 관례 : c[i]*/
operator fun Point.get(index: Int): Int {
    return when(index) {
        0 -> x
        1 -> y
        else ->
            throw IndexOutOfBoundsException("Invalid coordinate $index")
    }
}

data class MutablePoint(var x: Int, var y: Int)

operator fun MutablePoint.set(index: Int, value: Int) {
    when(index) {
        0 -> x = value
        1 -> y = value
        else ->
            throw IndexOutOfBoundsException("Invalid coordinate $index")
    }
}


/** 7.3.1 contains 관례 : in*/
data class Rectangle(val upperLeft: Point, val lowerRight: Point)

operator fun Rectangle.contains(p: Point): Boolean {
    return p.x in upperLeft.x until lowerRight.x &&
            p.y in upperLeft.y until lowerRight.y
}


/** 7.3.4 iterator */
operator fun ClosedRange<LocalDate>.iterator(): Iterator<LocalDate> =
        object : Iterator<LocalDate> {
            var current = start

            override fun hasNext() =
                    current <= endInclusive

            override fun next() = current.apply {
                current = plusDays(1)
            }
        }




/** 7.4.1 destructing declaration*/
data class NameComponents(val name: String, val extension: String)

fun splitFilename(fullName: String): NameComponents {
    val result = fullName.split('.', limit = 2)
    return NameComponents(result[0], result[1])
//    val (name, extension) = fullName.split('.', limit = 2)
//    return NameComponents(name, extension)
}

fun printEntries(map: Map<String, String>) { //iteration 안에서도 쓸 수 있다.
    for ((key, value) in map) {
        println("key: $key, value: $value")
    }
}



/** 7.5.1 lazy init */
class Email { /*...*/ }
fun loadEmails(crew: Crew): List<Email> {
    println("Load emails for ${crew.name}")
    return listOf(/*...*/)
}
class Crew(val name: String) {
    // backing property pattern (_emails: nullable, emails: read _emails)
    // 최초로 한번만 loadEmails 를 수행하지만 코드가 길고 thread shfe 하지 않다.
    private var _emails: List<Email>? = null

    val emails: List<Email>
        get() {
            if (_emails == null) {
                _emails = loadEmails(this)
            }
            return _emails!!
        }

    /** lazy~ backing property와 1번만 로딩하는 위 로직을 캡슐화
     * 인자 : 값을 초기화할때 사용할 람다
     * 기본적으로 thread safe
     * 필요에 따라 동기화에 사용할 lock 을 전달할 수도, 막을 수도 있다. */
    val lazyEmails by lazy { loadEmails(this) } // lazy 함수를 이용한 위임 프로퍼ㅌㅣ


}



