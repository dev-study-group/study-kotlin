package com.kakao.studykotlin

import org.junit.Test

class Chapter8HighOrderFunction {

    @Test
    fun basicHighOrderFunction() {
        val url = "localhost"
        performRequest(url) { code, content -> /*...*/ }    // callback 함수 lambda 로 정
    }

    @Test
    fun test_1_2() {
        loggingMod {a, b -> a % b}
    }


    @Test
    fun test_1_4() {
        val letters = listOf("Alpha", "Beta")
        println(letters.joinToString()) // parameter nullable
        println(letters.joinToString { it.toLowerCase() })  // use default values
        println(letters.joinToString(separator = "! ", postfix = "! ", transform = { it.toUpperCase() }))
    }

    @Test
    fun test_3_1() {
        lookForAlice(people)
        lookForAliceLamda(people)
    }

    @Test
    fun test_3_2() {
        println(StringBuilder().apply sb@{
            listOf(1, 2, 3).apply {
                this@sb.append(this.toString())
            }
        })
    }
}


fun normal(x: Int): Int{
    return x * 2
}

/** 8.1 고차함수 정의 */

/** 1.1 고차함수 : 매개변수나 리턴타입이 함수 */
fun performRequest(
        url: String,
        callback: (code: Int, content: String) -> Unit
) {
}


/** 1.2 매개변수 함수를 함수 내에서 사용 */
fun loggingMod(operation: (Int, Int) -> Int) {
    val result = operation(2, 3)
    println("The result is $result")
}


/** 1.3 자바에서 코틀린 함수 타입 사용
파라매터 22개의 처리를 위한 Functions 미리 구현됨
https://github.com/JetBrains/kotlin/blob/master/libraries/stdlib/jvm/runtime/kotlin/jvm/functions/Functions.kt

코틀린 함수를 자바 8 이상에서 사용시 : 람다
코틀린 함수를 자바 8 이전에서 사용시 : FunctionN 인터페이스를 이용해 무명 클래스 사용
 * */



/** 1.4 디폴트 값을 지정한 함수 타입 파라미터나 널이 될 수 있는 함수 타입 파라미터 */
fun <T> Collection<T>.joinToString(
        separator: String = ", ",
        prefix: String = "",
        postfix: String = "",
        transform: (T) -> String = { it.toString() }
): String {
    val result = StringBuilder(prefix)

    for ((index, element) in this.withIndex()) {
        if (index > 0) result.append(separator)
        result.append(transform(element))
    }

    result.append(postfix)
    return result.toString()
}


/** 1.5 함수에서 함수를 반환 */
enum class Delivery { STANDARD, EXPEDITED }

class Order(val itemCount: Int)

fun getShippingCostCalculator(delivery: Delivery): (Order) -> Double {
    // 반환 타입 : (Order) -> Double (함수)
    if (delivery == Delivery.EXPEDITED) {
        return { order -> 6 + 2.1 * order.itemCount }
    }

    return { order -> 1.2 * order.itemCount }
}


/** 8.2 인라인 함수 : 람다의 부가 비용 없애기 */

/** 2.1
 * 코틀린 람다가 변수를 포획하면 람다 생성 시점마다 새로운 무명클래스가 생성되어 부가 비용이든다.
 * inlining: inline 변경자를 붙이면 컴파일러는 그 함수를 호출하는 모든 문장을 함수 본문에 해당하는 바이트코드로 바꿔치기해준다.
 * p366
 *
 *2.4 함수를 인라인으로 선언해야 하는 경우
inline 키워드를 사용해도 람다를 인자로 받는 함수의 경우가 이익이 더 많다
함수 호출 비용을 낮춤
람다를 표현하는 클래스, 람다 인스턴스에 해당하는 객체 생성이 불필요
현재 JVM은 함수 호출과 람다를 인라이닝 불가능
일반 람다로는 사용 불가능한 기능 사용 가능 (non-local 등)
 * */



/** 2.3 collection inlining*/
data class P(val name: String, val age: Int)

val people = listOf(P("Alice", 29), P("Bob", 31))

fun main(args: Array<String>) {     // collection 을 리턴하는 함수는 inline
    println(people.filter { it.age < 30 })
}



/** 8.3 고차 함수 안에서 흐름 제어 */

/** 3.1  람다 안의 return문 : 람다를 둘러싼 함수로부터 반환 */
fun lookForAlice(people: List<P>) {
    for (person in people) {
        if (person.name == "Alice") {
            println("Found!")
            return
        }
    }
    println("Alice is not found")
}

fun lookForAliceLamda(people: List<P>) {
    people.forEach {
        if (it.name == "Alice") {
            println("Found!")
            return
        }
    }
    println("Alice is not found")
}

/** 3.2 람다의 리턴
 * forEach label@{ return@label }
 * forEach { return@forEach }
 * */


/** 3.3 무명 함수(Anonymous functions): 기본적으로 로컬 return
 * p380
 * */
fun lookForAliceAnonymousFun(people: List<P>) {
    people.forEach(fun (person) {
        if (person.name == "Alice") return // 무명함수의 return 은 가장 가까운 함수를 가리킴
        println("${person.name} is not Alice")
    })
}


